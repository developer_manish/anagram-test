<?php

class Anagram 
{
    /** @var string */
    private $word;

    /**
     * Anagram construct
     *
     * @param string $word
     */
    public function __construct(string $word)
    {
        $this->word = $word;
    }

    /**
     * Get the anagram list
     *
     * @return array
     */
    public function getAnagramList(): array
    {
        $anagrams = [];

        // split into array
        $charArr = str_split($this->word);

        $firstIndex = 0;
        $lastIndex = count($charArr) - 1;
        
        // implement swap logic
        for ($i = $firstIndex; $i <= $lastIndex; $i++ ) {
            $anagrams[] = implode($this->swap($charArr, $i, $lastIndex));
        }
        
        /** @todo */
        // implement permutation logic
        
        return $anagrams;  
    }

    /**
     * Swap position of the array elements
     *
     * @param array $charArr
     * @param integer $indexI
     * @param integer $indexJ
     * @return array
     */
    private function swap(array $charArr, int $indexI, int $indexJ): array
    {
        // termination logic
        if ($indexI == $indexJ) {
            return $charArr;
        }

        // get the elements
        $elementI = $charArr[$indexI];
        $elementJ = $charArr[$indexJ];

        // swap values
        $charArr[$indexI] = $elementJ;
        $charArr[$indexJ] = $elementI;
        
        return $this->swap($charArr, $indexI + 1, $indexJ);
    }
}