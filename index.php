<?php

require 'classes/Anagram.php';

// take input
do {
    $string = readline('Enter your string: ');
} while (empty(trim($string)));

$start = microtime(true);

// compute all anagram strings
$anagram = new Anagram($string);
$anagramList = $anagram->getAnagramList();

// print anagram strings
foreach ($anagramList as $word) {
    /** @todo */
    // check if word exists in the list or not
    echo $word . PHP_EOL;
}

$timeTaken = microtime(true) - $start;

echo "Time taken: ${timeTaken} micro seconds";